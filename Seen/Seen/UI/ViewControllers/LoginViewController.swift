//
//  LoginViewController.swift
//  Seen
//
//  Created by Tobias Boenisch on 09.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var emailTF: UITextField!
    @IBOutlet weak var pwTF: UITextField!
    @IBOutlet weak var login_button: UIButton!
    @IBOutlet weak var invalidMail: UILabel!
    @IBOutlet weak var invalidPW: UILabel!
    
    
    var fb: Firebaser!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invalidMail.isHidden = true
        invalidPW.isHidden = true
        self.fb = Firebaser()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValid(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func wrongMail () {
        invalidMail.isHidden = false
    }
    
    func wrongPW () {
        invalidPW.isHidden = false
    }
    

    @IBAction func loginBtnClicked(_ sender: UIButton) {
        invalidMail.isHidden = true
        invalidPW.isHidden = true
        let passwordVar = pwTF.text
        let emailVar = emailTF.text
        
        if  isValid(emailVar!){
            
            Auth.auth().signIn(withEmail: emailVar!, password: passwordVar!) { (user, error) in

                if let error = error {
                    self.wrongPW()
                    return
                }
                self.performSegue(withIdentifier: "showCamera", sender: self)
            }
            
        }
        else {wrongMail()}
    }
}
