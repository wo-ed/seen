//
//  ViewController.swift
//  Seen
//
//  Created by Mo on 07.11.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import ARCL
import CoreLocation
import MapKit
import Pulsator
import Firebase
import SpriteKit

class CameraViewController: UIViewController, ARSCNViewDelegate, SCNPhysicsContactDelegate, CLLocationManagerDelegate, MKMapViewDelegate, WeaponMenuViewDelegate, UIGestureRecognizerDelegate,
RGPReloadAnimatorDelegate {
    
    private let sceneLocationView = SceneLocationView()
    private var lifeBarScene: LifebarScene!
    
    private var assets: Assets!
    
    private var slingShot: Slingshot!
    private var rpg: RPG!
    private var activeWeapon: Weapon?
    
    @IBOutlet weak var hudView: HUDView!
    @IBOutlet weak var weaponMenuView: WeaponMenuView!
    
    private var edgePan: UIScreenEdgePanGestureRecognizer!
    
    private var progressIndicator: UIActivityIndicatorView!
    
    // Game Over Popup
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var theVisualEffectView: UIVisualEffectView!
    var effect:UIVisualEffect!
    
    // Boss coordinates.
    var bossCoords = [CLLocationCoordinate2D]()
    
    private var bossName: String?
    
    // A timer updating the heading annotations every second.
    var headingAnnotationTimer: Timer!
    
    // A timer updating the user's coordinates.
    private var coordsTimer: Timer!
    private let coordsTimerFrequency = 10.0
    private let distanceDelta = 10.0 // Meters
    
    // Interface to backend communication.
    private let fb = Firebaser()
    
    private let locationManager = CLLocationManager()
    private var lastLocation: CLLocation?
    
    // The boss at the static position
    private var staticDragon: Dragon?
    
    // Determines the behavior of the boss at the static position
    private let bossTestMode: BossTestMode = .hidden
    
    private var attackHandle: DatabaseHandle?
    private var bossHpHandle: DatabaseHandle?
    
    private var playerBody: PlayerBody!
    
    private var dragons = [String: Dragon]()
    
    private var playerHp = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        
        //Game Over Screen Vorbereitungen
        effect = theVisualEffectView.effect
        theVisualEffectView.effect = nil
        popUpView.layer.cornerRadius = 5
        
        // Adjust HUD to heading direction.
        locationManager.startUpdatingHeading()
    
        assets = Assets()
        lifeBarScene = LifebarScene(size: self.view.bounds.size)
        playerBody = PlayerBody(sceneLocationView: sceneLocationView, assets: assets)
        slingShot = Slingshot(sceneLocationView: sceneLocationView, assets: assets)
        rpg = RPG(sceneLocationView: sceneLocationView, assets: assets)
        
        initSceneLocationView()
        
        if(bossTestMode != .hidden) {
            initStaticDragon()
        }
        
        activateWeapon(index: 1)
        
        hudView.delegate = self
        startListenForBosses()
        
        weaponMenuView.delegate = self
        // Checks if user is in range of a boss.
        startListenForFight()
        
        headingAnnotationTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(updateHeadingAnnotations), userInfo: nil, repeats: true)
        
        coordsTimer = Timer.scheduledTimer(timeInterval: coordsTimerFrequency, target: self, selector: #selector(updateUserLocation), userInfo: nil, repeats: true)

        lifeBarScene.isUserInteractionEnabled = false
        lifeBarScene.updateLifebar(playerHp)
        
        if(bossTestMode == .attackingPlayer) {
            staticDragon?.startDemoAttack(timeInterval: staticDragon!.fireballAttackDelay)
        }

        // Init swipe recognizer
        edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        edgePan.delegate = self
        sceneLocationView.addGestureRecognizer(edgePan)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.delegate = self
        tapRecognizer.require(toFail: edgePan)
        sceneLocationView.addGestureRecognizer(tapRecognizer)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressRecognizer.minimumPressDuration = 0
        longPressRecognizer.delegate = self
        longPressRecognizer.require(toFail: edgePan)
        sceneLocationView.addGestureRecognizer(longPressRecognizer)
        
        progressIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        progressIndicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        progressIndicator.center = self.view.center
        view.addSubview(progressIndicator)
        
        rpg.delegate = self
    }
    
    // Game Over
    //Blurr Effect + Popup
    
    func gameOverIn() {
        self.view.addSubview(theVisualEffectView)
        self.view.addSubview(popUpView)
        popUpView.center = self.view.center
        popUpView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        popUpView.alpha = 0
        
        UIView.animate(withDuration: 0.5){
            self.theVisualEffectView.effect = self.effect
            self.popUpView.alpha = 1
            self.popUpView.transform = CGAffineTransform.identity
            }
    }
    
    func gameOverOut() {
        UIView.animate(withDuration: 0.1, animations: {
            self.popUpView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.popUpView.alpha = 0
            self.theVisualEffectView.effect = nil
        }) { (sucess:Bool) in
            self.popUpView.removeFromSuperview()
        }
        
        
    }
    
    
    func gameOver() {
        //gibt es etwas mit dem man die Aktualisierung von
        //Drache usw stoppen kann?
        
        activeWeapon?.deactivate()
        self.lifeBarScene.isHidden = true
        self.hudView.isHidden = true
        
        
        DispatchQueue.main.async {
            self.gameOverIn()
        }
            }
    
    
    //Leave Button Clicked

    @IBAction func clickedLeaveButton(_ sender: Any) {
        gameOverOut()
        if fb.signOut() {
        }
        performSegue(withIdentifier: "showLogin3", sender: self)
        
    }
    
    
    
    
    // Game Over Ende
    
    
    
    private func initSceneLocationView() {
        view.addSubview(sceneLocationView)
        sceneLocationView.scene.physicsWorld.contactDelegate = self
        sceneLocationView.isUserInteractionEnabled = true
        sceneLocationView.delegate = self
        sceneLocationView.overlaySKScene = self.lifeBarScene
        //sceneLocationView.debugOptions = .showPhysicsShapes
    }
    
    // Places a dragon at a predefined static position
    private func initStaticDragon() {
        staticDragon = Dragon("static", assets, sceneLocationView)
        staticDragon!.rootNode.position = SCNVector3Make(0, 0, -10)
        sceneLocationView.scene.rootNode.addChildNode(staticDragon!.rootNode)
    }
    
    private func addOrUpdateDragon(bossName: String, location: CLLocation) {
        if let dragon = getDragon(bossName: bossName) {
            dragon.locationNode!.location = location
            sceneLocationView.updatePositionAndScaleOfLocationNode(locationNode: dragon.locationNode!)
            
            print("boss position updated")
        } else {
            let dragon = Dragon(bossName, assets, sceneLocationView)
            dragon.locationNode = Location3DNode(location: location, annotationNode: dragon.rootNode)
            sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: dragon.locationNode!)
            dragons[bossName] = dragon
            
            print("new boss added")
        }
    }
    
    private func getDragon(bossName: String) -> Dragon? {
        return dragons[bossName]
    }
    
    private func removeDragon(bossName: String) {
        if let dragon = dragons[bossName] {
            removeDragon(dragon: dragon)
        }
    }
    
    private func removeDragon(dragon: Dragon) {
        sceneLocationView.removeLocationNode(locationNode: dragon.locationNode!)
        dragons.removeValue(forKey: dragon.bossName)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sceneLocationView.run()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sceneLocationView.frame = view.bounds
    }
    
    private func activateWeapon(index: Int) {
        activeWeapon?.deactivate()
        switch index {
        case 0:
            activeWeapon = slingShot
        case 1:
            activeWeapon = rpg
        default:
            activeWeapon = rpg
        }
        
        activeWeapon!.activate()
    }
    
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        if(playerHp <= 0) {
            return
        }
        
        activeWeapon?.physicsWorld(world, didBegin: contact)
        playerBody?.physicsWorld(world, didBegin: contact)
        
        if(contact.nodeA.physicsBody!.categoryBitMask == Categories.playerBody &&
            contact.nodeB.physicsBody!.categoryBitMask == Categories.fireball) {
            
            handlePlayerHit(contact: contact)
            
        } else if(contact.nodeA.physicsBody!.categoryBitMask == Categories.boss &&
            contact.nodeB.physicsBody!.categoryBitMask == Categories.missile) {
            
            handleBossHit(contact: contact)
        }
    }
    
    private func handlePlayerHit(contact: SCNPhysicsContact) {
        // Disable the missile to prevent multiple damage
        contact.nodeB.physicsBody!.categoryBitMask = 0
        
        playerHp -= 20
        lifeBarScene.updateLifebar(playerHp)
        
        if(playerHp <= 0) {
            DispatchQueue.main.async {
                self.gameOver()
            }
        }
        print("Hit > player")
    }
    
    private func handleBossHit(contact: SCNPhysicsContact) {
        // Disable the fireball to prevent multiple damage
        contact.nodeB.physicsBody!.categoryBitMask = 0
        
        if let bossName = self.bossName {
            fb.decrementHpFrom(boss: bossName, hp: 10)
            
            print("Hit > \(bossName)")
        } else {
            print("Hit > Unknown boss")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneLocationView.pause()
        // Stop listening to database.
        fb.getDatabaseReference().removeAllObservers()
        view.removeGestureRecognizer(edgePan)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        sceneLocationView.renderer(renderer, didRenderScene: scene, atTime: time)
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        sceneLocationView.session(session, didFailWithError: error)
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        sceneLocationView.sessionWasInterrupted(session)
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        sceneLocationView.sessionInterruptionEnded(session)
    }
    
    @objc private func updateUserLocation() {
        let loc = locationManager.location
        let c = loc?.coordinate
        let alt = locationManager.location?.altitude
        
        // Only update location when app just started or distance changed significantly.
        if lastLocation == nil || lastLocation!.distance(from: loc!) > distanceDelta {
            lastLocation = loc!
            let success = fb.updateUserLocation(longitude: c!.longitude, latitude: c!.latitude, altitude: alt!)
            if success {
                print("Location updated: \(loc!)")
            }
        }
        
        
    }
    
    // Checks backend for bosses to display.
    private func startListenForBosses() {
        let db = fb.getDatabaseReference()
        
        // Initialize markers on HUDView and bosses the first time.
        db.child("bosses").observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let bossSnapshot = enumerator.nextObject() as? DataSnapshot {
                let name = bossSnapshot.key as String
                let boss = bossSnapshot.value as? NSDictionary
                let coords = boss?["coords"] as? NSDictionary
                if coords == nil {
                    continue
                }
                let latitude = coords?["latitude"] as! Double
                let longitude = coords?["longitude"] as! Double
                let altitude = coords?["altitude"] as! Double
                
                // Create boss instance.
                let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let location = CLLocation(coordinate: coordinate, altitude: altitude)
                self.addOrUpdateDragon(bossName: name, location: location)
                
                self.hudView.addBossAnnotation(to: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), name: name)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Update
        db.child("bosses").observe(.childChanged, with: { (snapshot) in
            let name = snapshot.key
            print("New Boss")
            let coordSnap = snapshot.childSnapshot(forPath: "coords")
            let coords = coordSnap.value as? NSDictionary
            let latitude = coords?["latitude"] as! Double
            let longitude = coords?["longitude"] as! Double
            let altitude = coords?["altitude"] as! Double
            
            print("Boss Long: \(longitude)")
            print("Boss Lat: \(latitude)")
            print("Boss Alt: \(altitude)")
            
            // Create boss instance.
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let location = CLLocation(coordinate: coordinate, altitude: altitude)
            self.addOrUpdateDragon(bossName: name, location: location)
            
            var tmp = [HUDView.BossAnnotation]()
            for boss in self.hudView.bossAnnotations {
                if boss.title != name {
                    tmp.append(boss)
                } else {
                    self.hudView.removeAnnotation(boss)
                    var tmp1 = [HUDView.HeadingAnnotation]()
                    for heading in self.hudView.headingAnnotations {
                        if boss == heading.headsTo {
                            self.hudView.removeAnnotation(heading)
                        } else {
                            tmp1.append(heading)
                        }
                    }
                    self.hudView.headingAnnotations = tmp1
                }
            }
            self.hudView.bossAnnotations = tmp
            self.hudView.addBossAnnotation(to: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), name: name)
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Delete
        db.child("bosses").observe(.childRemoved, with: { (snapshot) in
            let name = snapshot.key
            print("Boss removed: \(name)")
            self.removeDragon(bossName: name)
            // Remove annotations.
            for boss in self.hudView.annotations {
                if boss.title! == name {
                    self.hudView.removeAnnotation(boss)
                    print("Boss deleted. Annotation removed.")
                }
            }
            for boss in self.hudView.headingAnnotations {
                if boss.headsTo.title == name {
                    self.hudView.removeAnnotation(boss)
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // If the player is in range of a boss, the HUDView is hidden.
    private func startListenForFight() {
        let db = fb.getDatabaseReference()
        db.child("players").child(fb.getUserID()).child("fight").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                print("Player \(self.fb.getUserID()) is in fight.")

                let bossName = snapshot.value as! String
                self.bossName = bossName
                self.attackHandle = self.startListenForAttack(bossName: bossName)
                self.bossHpHandle = self.startListenForBossHp(bossName: bossName)
                print("Player is in fight:  \(bossName)")
                self.adaptGui(inFight: true)
            } else {
                
                // Stop listening for attacks.
                if self.attackHandle != nil {
                    db.removeObserver(withHandle: self.attackHandle!)
                    db.removeObserver(withHandle: self.bossHpHandle!)
                }
                self.adaptGui(inFight: false)
            }
        })
    }
    
    private func adaptGui(inFight: Bool) {
        if inFight {
            // Hide HUD
            hudView.isHidden = true
            
            // Show Lifebar of the Boss
            self.lifeBarScene.showBossBar()
        } else {
            // Show HUD
            hudView.isHidden = false
            view.bringSubview(toFront: hudView)
            
            // Hide Lifebar of the Boss
            self.lifeBarScene.hideBossBar()
        }
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            print("Screen edge swiped!")
            weaponMenuView.slideInFromLeft()
            weaponMenuView.isHidden = false
            view.bringSubview(toFront: weaponMenuView)
        }
    }
    
    @objc private func handleLongPress(sender: UILongPressGestureRecognizer) {
        if(activeWeapon === slingShot) {
            if sender.state == .began && !slingShot.isTargeting {
                slingShot.startTargeting()
            }
            
            if sender.state == .ended && slingShot.isTargeting{
                slingShot.shoot()
            }
        }
    }
    
    @objc private func handleTap(sender: UITapGestureRecognizer) {
        if activeWeapon === rpg && sender.state == .ended && rpg.isShootingAllowed {
            rpg.shoot()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    private func startListenForBossHp(bossName: String) -> DatabaseHandle {
        let db = fb.getDatabaseReference()
        return db.child("bosses").child(bossName).child("hp").observe(.value, with: { (snapshot) in
            if !snapshot.exists() {
                return
            }
            let hp = snapshot.value as! Int
            print("Boss: \(bossName), Hp: \(hp)")
            self.lifeBarScene.updateBossbar(hp)
            

            if hp <= 0 {
                self.fb.removeFightFromCurrentUser()
            }
            // TODO: hp <= 0
        })
    }
    
    private func startListenForAttack(bossName: String) -> DatabaseHandle {
        print("Listening for attacks...")
        // TODO
        let db = fb.getDatabaseReference()
        return db.child("bosses").child(bossName).child("target").observe(.value, with: { (snapshot) in
            if !snapshot.exists() {
                return
            }
            let target = snapshot.value as! String
            db.child("players").child(target).child("coords").observeSingleEvent(of: .value, with: { (snapshot) in
                let coords = snapshot.value as? NSDictionary
                let latitude = coords?["latitude"] as! Double
                let longitude = coords?["longitude"] as! Double
                let altitude = coords?["altitude"] as! Double
                let targetCoords = SCNVector3Make(Float(longitude), Float(latitude), Float(altitude))
                
                print("Boss Attack Target: \(target)")
                print("Target location: \(targetCoords)")
                
                if let dragon = self.getDragon(bossName: bossName) {
                    if target == self.fb.getUserID() {
                        dragon.attackPlayer()
                        self.handleAttackDelay(delay: dragon.fireballAttackDelay, bossName: bossName)
                    } else {
                        dragon.attackOtherPlayer(latitude: latitude, longitude: longitude, altitude: altitude)
                    }
                }
            })
        })
    }
    
    @objc private func updateHeadingAnnotations() {
        // Remove heading annotations for bosses that are currently inside map camera.
        for annotation in hudView.headingAnnotations {
            if(MKMapRectContainsPoint(hudView.visibleMapRect, MKMapPointForCoordinate(annotation.headsTo.coordinate))) {
                hudView.removeAnnotation(annotation)
            }
        }
        hudView.headingAnnotations = hudView.headingAnnotations.filter { !MKMapRectContainsPoint(hudView.visibleMapRect, MKMapPointForCoordinate($0.headsTo.coordinate))}
        
        outer: for boss in hudView.bossAnnotations {
            // Is boss currently visible?
            if(MKMapRectContainsPoint(hudView.visibleMapRect, MKMapPointForCoordinate(boss.coordinate))) {
                continue
            }
            // Is heading annotation already visible?
            for annotation in hudView.headingAnnotations {
                if(annotation.headsTo === boss) {
                    continue outer
                }
            }
            let curLocation = hudView.userLocation.coordinate
            let headingAngle = hudView.getHeadingForCoords(from: curLocation, to: boss.coordinate)
            let res = hudView.getPointOnCircleByAngle(origin: curLocation, radiansAngle: headingAngle)
            
            // Draw line to boss from current location.
            //let lineCoords:[CLLocationCoordinate2D] = [hudView.userLocation.coordinate, coord]
            //let line = MKPolyline(coordinates: lineCoords, count: lineCoords.count)
            let annotation = HUDView.HeadingAnnotation(boss: boss)
            annotation.coordinate = res
            //annotation.rotate(radians: headingAngle)
            hudView.addAnnotation(annotation)
            hudView.headingAnnotations.append(annotation)
        }
    }
    
    // MARK: LocationManager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        // Adjust map to compass.
        hudView.camera.heading = newHeading.magneticHeading
        hudView.setCamera(hudView.camera, animated: true)
        hudView.setUserTrackingMode(MKUserTrackingMode.follow, animated: false)
        updateHeadingAnnotations()
    }
    
    // MARK: MapView delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is HUDView.ImageAnnotation){
            return nil
        }
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = false
        }
        else {
            anView!.annotation = annotation
        }
        
        // Add picture
        let ba = annotation as! HUDView.ImageAnnotation
        
        anView!.image = ba.img
        
        // Add pulse animation for bosses
        if annotation is HUDView.BossAnnotation {
            let pulsator = Pulsator()
            pulsator.backgroundColor = UIColor.red.cgColor
            pulsator.position = CGPoint(x: ba.img.size.height/2, y: ba.img.size.width/2)
            anView!.layer.addSublayer(pulsator)
            pulsator.start()
            hudView.bossAnnotations.append(annotation as! HUDView.BossAnnotation)
            
            anView!.canShowCallout = false
        }
        return anView
    }
    
    public func handleAttackDelay(delay: Double, bossName: String) {
        Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(handleAttackDelayHelper), userInfo: bossName, repeats: false)
    }
    
    @objc public func handleAttackDelayHelper(timer: Timer) {
        let bossName = timer.userInfo as! String
        fb.notifyAttackIsOver(boss: bossName)
    }
    
    func weaponChangedTo(weapon: Int) {
        activateWeapon(index: weapon)
    }
    
    func startRPGReloadAnimation() {
        DispatchQueue.main.async {
            self.progressIndicator.startAnimating()
        }
    }
    
    func stopRPGReloadAnimation() {
        DispatchQueue.main.async {
            self.progressIndicator.stopAnimating()
        }
    }
}
