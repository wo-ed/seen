//
//  LogoutViewController.swift
//  Seen
//
//  Created by Tobias Boenisch on 16.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import UIKit

class LogoutViewController: UIViewController {

    var fb: Firebaser!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fb = Firebaser()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    @IBAction func LogoutClicked(_ sender: Any) {
        if fb.signOut() {
            performSegue(withIdentifier: "showLogin", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
