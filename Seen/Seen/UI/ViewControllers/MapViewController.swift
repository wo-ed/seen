//
//  MapViewController.swift
//  Seen
//
//  Created by Mo on 23.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class MapViewController: UIViewController, MKMapViewDelegate {

    private let fb = Firebaser()
    private let locationManager = CLLocationManager()
    
    private let zoomFactor = 0.02
    
    @IBOutlet weak var mapView: MapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Zoom map camera in.
        let location = locationManager.location!
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: zoomFactor, longitudeDelta: zoomFactor))
        
        self.mapView.setRegion(region, animated: true)
        print("Map adjusted.")
        
        mapView.delegate = self

        
        // Display bosses on the map.
        startListenForBosses()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // Checks backend for bosses to display.
    private func startListenForBosses() {
        let db = self.fb.getDatabaseReference()
        
        // Initialize annotations the first time.
        db.child("bosses").observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let bossSnapshot = enumerator.nextObject() as? DataSnapshot {
                let bossName = bossSnapshot.key as String
                let boss = bossSnapshot.value as? NSDictionary
                let coords = boss?["coords"] as? NSDictionary
                if coords == nil {
                    continue
                }
                let latitude = coords?["latitude"] as! Double
                let longitude = coords?["longitude"] as! Double
                
                self.mapView.addBossAnnotation(to: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), name: bossName)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Listen for changes.
        db.child("bosses").observe(.childChanged, with: { (snapshot) in
            let name = snapshot.key
            print("New Boss")
            let coordSnap = snapshot.childSnapshot(forPath: "coords")
            let coords = coordSnap.value as? NSDictionary
            let latitude = coords?["latitude"] as! Double
            let longitude = coords?["longitude"] as! Double
            
            print("Boss Long: \(longitude)")
            print("Boss Lat: \(latitude)")
            
            // Remove old annotation.
            self.mapView.removeBossAnnotation(name: name)
            // Add new one.
            self.mapView.addBossAnnotation(to: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), name: name)
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Delete
        db.child("bosses").observe(.childRemoved, with: { (snapshot) in
            let name = snapshot.key
            print("Boss removed: \(name)")
            // Remove annotations.
            self.mapView.removeBossAnnotation(name: name)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    // MARK: MapView delegate (change image of annotations)
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is HUDView.ImageAnnotation){
            return nil
        }
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation
        }
        
        // Add picture
        let ba = annotation as! HUDView.ImageAnnotation
        anView!.image = ba.img
        return anView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.red
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKOverlayRenderer()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
}
