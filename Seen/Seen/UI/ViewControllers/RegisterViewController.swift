//
//  RegisterViewController.swift
//  Seen
//
//  Created by Tobias Boenisch on 16.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var mailTF: UITextField!
    @IBOutlet weak var pwTF: UITextField!
    @IBOutlet weak var confirmPwTF: UITextField!
    @IBOutlet weak var invalidMailLabel: UILabel!
    @IBOutlet weak var invalidPWLabel: UILabel!
    @IBOutlet weak var shortPWLabel: UILabel!
    
    
    var fb: Firebaser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invalidMailLabel.isHidden = true
        invalidPWLabel.isHidden = true
        shortPWLabel.isHidden = true
        self.fb = Firebaser()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValid(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func pwValid(_ passwordCheck: String) -> Bool {
        let pwCompareVar = "^(?=.*[a-z])[A-Za-z\\1-9\\d$@$#!%*?&]{8,}"
        let pwTest = NSPredicate(format:"SELF MATCHES[c] %@", pwCompareVar)
        return pwTest.evaluate(with: passwordCheck)
    }

    
    func wrongMail () {
        invalidMailLabel.isHidden = false
    }
    
    func wrongPW () {
        invalidPWLabel.isHidden = false
    }
    
    func shortPW () {
        shortPWLabel.isHidden = false
    }
    
    @IBAction func RegisterButtonClicked(_ sender: Any) {
        invalidMailLabel.isHidden = true
        invalidPWLabel.isHidden = true
        shortPWLabel.isHidden = true
        let passwordVar = pwTF.text
        let emailVar = mailTF.text
        let confirmVar = confirmPwTF.text
//        let usernameVar = usernameTF.text
        
        if pwValid(passwordVar!) {
            if confirmVar==passwordVar {
                if  isValid(emailVar!){
                    fb.registerUser(mail: emailVar!, pw: passwordVar!)
                    performSegue(withIdentifier: "showCamera2", sender: self)
                    
                }
                else {wrongMail()}
            }
            else {wrongPW()}
            
        }
        else {shortPW()}



    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
