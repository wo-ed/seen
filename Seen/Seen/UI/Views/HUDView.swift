//
//  RadarView.swift
//  FirebaseTest
//
//  Created by Mo on 05.12.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class HUDView: MKMapView {
    
    
    class ImageAnnotation: MKPointAnnotation {
        var img : UIImage!
        
        func rotate(radians: Double) {
            var newSize = CGRect(origin: CGPoint.zero, size: img.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
            //Trim off the extremely small float value to prevent core graphics from rounding it up
            newSize.width = floor(newSize.width)
            newSize.height = floor(newSize.height)
            
            UIGraphicsBeginImageContext(newSize);
            let context = UIGraphicsGetCurrentContext()!
            
            //Move origin to middle
            context.translateBy(x: newSize.width/2, y: newSize.height/2)
            //Rotate around middle
            context.rotate(by: CGFloat(radians))
            
            img.draw(in: CGRect(x: -img.size.width/2, y: -img.size.height/2, width: img.size.width, height: img.size.height))
            
            img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
    }
    
    class BossAnnotation: ImageAnnotation {
        init(name: String) {
            super.init()
            super.title = name
            super.img = #imageLiteral(resourceName: "bossOverlay")
        }
    }
    
    class HeadingAnnotation: ImageAnnotation {
        var headsTo : BossAnnotation
        init(boss: BossAnnotation) {
            headsTo = boss
            super.init()
            super.img = #imageLiteral(resourceName: "headingIcon")
            
        }
    }
    
    
    // MARK: Properties
    // List of all visible boss annotations.
    var bossAnnotations = [HUDView.BossAnnotation]()
    // List of all visible heading annotations.
    var headingAnnotations = [HUDView.HeadingAnnotation]()
    
    // Initial map adjustment.
    let testCoord = CLLocationCoordinate2D(latitude: 48.106835, longitude: 11.701007)
    let testCoord2 = CLLocationCoordinate2D(latitude: 48.105835, longitude: 11.707007)
    let testCoord3 = CLLocationCoordinate2D(latitude: 48.10770, longitude: 11.698339)
    
    let borderAlphaVal: CGFloat = 1.0
    let borderColor = UIColor.darkGray
    let borderWidth: CGFloat = 2.5
    
    // Fixed radius due to static map size.
    let r = 1750.0
    
    // Used for user location
    let locationManager = CLLocationManager()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Even circle.
        layer.cornerRadius = (frame.size.width/2 + frame.size.height/2)/2
        clipsToBounds = true
        let grayAlpha = borderColor.withAlphaComponent(borderAlphaVal)
        layer.borderColor = grayAlpha.cgColor
        layer.borderWidth = borderWidth
   
        userLocation.title = ""
    }
    
    func getHeadingForCoords(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> Double {
        let fLatRad = from.latitude * .pi / 180.0
        let fLngRad = from.longitude * .pi / 180.0
        let tLatRad = to.latitude * .pi / 180.0
        let tLngRad = to.longitude * .pi / 180.0
        
        return atan2(sin(tLngRad-fLngRad)*cos(tLatRad), cos(fLatRad)*sin(tLatRad)-sin(fLatRad)*cos(tLatRad)*cos(tLngRad-fLngRad));
    }
    
    // Returns a point on the MapView bordercircle to visualize the heading to the boss.
    func getPointOnCircleByAngle(origin: CLLocationCoordinate2D, radiansAngle: Double) -> CLLocationCoordinate2D {
        let originPoint = MKMapPointForCoordinate(origin)
        let originX = originPoint.x
        let originY = originPoint.y
        
        // Rotate 90 degrees (pi/2) to the left.
        let x = originX + r * cos(radiansAngle-(.pi/2.0))
        print("X: \(x)")
        let y = originY + r * sin(radiansAngle-(.pi/2.0))
        print("y: \(y)")
        return MKCoordinateForMapPoint(MKMapPointMake(x, y))
    }
    
    
    func addBossAnnotation(to: CLLocationCoordinate2D, name: String) {
        let annotation = BossAnnotation(name: name)
        annotation.coordinate = to
        addAnnotation(annotation)
    }
}
