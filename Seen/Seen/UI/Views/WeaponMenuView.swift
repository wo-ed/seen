//
//  WeaponMenuView.swift
//  Seen
//
//  Created by Mo on 13.12.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import UIKit

class WeaponMenuView: UIView {
    
    var w1Btn: UIButton!
    var w2Btn: UIButton!
    
    var delegate : WeaponMenuViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setLayout()
        
        let label = UILabel()
        label.text = "Weapons:"
        label.font = UIFont(name: "Nasalization", size: 12)
        
        w1Btn = UIButton(type: .custom)
        w1Btn.setImage(#imageLiteral(resourceName: "slinghot"), for: .normal)
        w1Btn.addTarget(self, action: #selector(WeaponMenuView.buttonClicked(_:)), for: .touchUpInside)
        w2Btn = UIButton(type: .custom)
        w2Btn.setImage(#imageLiteral(resourceName: "bazooka"), for: .normal)
        w2Btn.addTarget(self, action: #selector(WeaponMenuView.buttonClicked(_:)), for: .touchUpInside)
        
        
        let container = UIStackView()
        container.addArrangedSubview(label)
        container.addArrangedSubview(w1Btn)
        container.addArrangedSubview(w2Btn)
        container.axis  = .vertical
        container.alignment = .fill
        container.distribution  = .equalSpacing
        container.translatesAutoresizingMaskIntoConstraints = false
        container.spacing = 10.0
        //Padding
        container.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        container.isLayoutMarginsRelativeArrangement = true
        
        //scrollView.addSubview(container)
        self.addSubview(container)
        //Constraints
        //scrollView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        //scrollView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        container.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        container.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    }
    
    @objc func buttonClicked(_ sender: AnyObject?) {
        if sender === w1Btn {
            delegate?.weaponChangedTo(weapon: 0)
        } else if sender === w2Btn {
            delegate?.weaponChangedTo(weapon: 1)
        }
        slideOutToLeft()
        isHidden = true
    }
    
    private func setLayout() {
        // Round corners
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 10, height: 10))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
        // Background color
        
        //layer.backgroundColor = bgColor.cgColor
        
        // Border and background
        //layer.borderColor = UIColor.black.cgColor
        //layer.borderWidth = 2.0
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path // Reuse the Bezier path
        let bgColor = UIColor.darkGray.withAlphaComponent(0.6)
        borderLayer.fillColor = bgColor.cgColor
        borderLayer.strokeColor = UIColor.darkGray.cgColor
        borderLayer.lineWidth = 3.5
        borderLayer.frame = self.bounds
        layer.addSublayer(borderLayer)
    }
    
    func slideInFromRight() {
        slide(transitionType: kCATransitionFromRight)
    }
    
    func slideOutToRight() {
        slide(transitionType: kCATransitionFromLeft)
    }
    
    func slideInFromLeft() {
        slide(transitionType: kCATransitionFromLeft)
    }
    
    func slideOutToLeft() {
        slide(transitionType: kCATransitionFromRight)
    }
    
    
    private func slide(transitionType: String, duration: TimeInterval = 0.8, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let transition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            transition.delegate = delegate as? CAAnimationDelegate
        }
        
        // Customize the animation's properties
        transition.type = kCATransitionPush
        transition.subtype = transitionType
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(transition, forKey: "slide")
    }
}

public protocol WeaponMenuViewDelegate {
    func weaponChangedTo(weapon: Int)
}

