//
//  MapView.swift
//  Seen
//
//  Created by Mo on 02.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase

class MapView: MKMapView {
    
    // Boss markers on the map mapped to their fight zone radius.
    private var bossAnnotations = [HUDView.BossAnnotation: MKCircle]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
        /*
        // Get bosses to display corresponding annotiations on the map.
        ref = Database.database().reference()
        
        ref.child("bosses").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let enumerator = snapshot.children
            while let bossSnapshot = enumerator.nextObject() as? DataSnapshot {
                //let childSnapshot = snapshot.childSnapshotForPath(child.key)
                let bossName = bossSnapshot.key as String
                print(bossName)
                let boss = bossSnapshot.value as? NSDictionary
                let hp = boss?["hp"] as? Int ?? 0
                print("HP: \(hp)")
                let coords = boss?["coords"] as? NSDictionary
                let latitude = coords?["latitude"] as? Double ?? 0
                print("Lat: \(latitude)")
                let longitude = coords?["longitude"] as? Double ?? 0
                print("Long: \(longitude)")
                let b = Boss(hp: hp, name: bossName, location: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                self.bosses.append(b)
                
                // Show some debug annotation
                let annotation = HUDView.BossAnnotation(name: bossName)
                annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                self.addAnnotation(annotation)
                
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        */
    }
    
    func addBossAnnotation(to: CLLocationCoordinate2D, name: String) {
        let annotation = HUDView.BossAnnotation(name: name)
        annotation.coordinate = to
        addAnnotation(annotation)
        
        let circle = MKCircle(center: to, radius: 300 as CLLocationDistance)
        add(circle)
        bossAnnotations[annotation] = circle
    }
    
    func removeBossAnnotation(name: String) {
        for boss in annotations {
            if boss.title! == name {
                removeAnnotation(boss)
                let ba = boss as! HUDView.BossAnnotation
                remove(bossAnnotations[ba]!)
                bossAnnotations.removeValue(forKey: ba)
            }
        }
    }
    
    
    
}
