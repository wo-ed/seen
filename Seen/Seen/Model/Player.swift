//
//  Player.swift
//  Seen
//
//  Created by admin on 09.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import Foundation
import MapKit

class Player {
    
    var hp: Int!
    var id: String
    
    init(hp: Int, id: String) {
        self.hp = hp
        self.id = id
    }
}
