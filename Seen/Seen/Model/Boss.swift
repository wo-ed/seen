//
//  Boss.swift
//  Seen
//
//  Created by Mo on 02.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import Foundation
import MapKit

class Boss {
    
    var hp: Int!
    var name: String!
    var location: CLLocationCoordinate2D!
    
    init(hp: Int, name: String, location: CLLocationCoordinate2D) {
        self.hp = hp
        self.name = name
        self.location = location
    }
}
