//
//  Util.swift
//  Seen
//
//  Created by Mo on 02.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import Firebase
import SceneKit

class Firebaser {
    
    var ref: DatabaseReference!
    
    init() {
        ref = Database.database().reference()
    }
    
    public func getDatabaseReference() -> DatabaseReference {
        return ref
    }
    
    public func registerUser(mail: String, pw: String) {
        Auth.auth().createUser(withEmail: mail, password: pw) { (user, error) in
            if error == nil {
                print("Registered successfully.")
            } else {
                print("Registration error occurred: \(error!)")
            }
        }
    }
    
    public func signIn(mail: String, pw: String) -> Bool {
        
        Auth.auth().signIn(withEmail: mail, password: pw) { (user, error) in
        }
        if Auth.auth().currentUser == nil {
            print("Login failed.")
            return false
        } else {
            print("Logged in successfully.")
            return addPlayer()
        }
    }
    
    public func signOut() -> Bool {
        ref.removeAllObservers()
        if removePlayer(uid: (Auth.auth().currentUser?.uid)!) {
            do {
                try Auth.auth().signOut()
                
                return true
            } catch {
                return false
            }
        }
        return false;
    }
    
    public func updateUserLocation(longitude: Double, latitude: Double, altitude: Double) -> Bool {
        if Auth.auth().currentUser == nil {
            // User is not signed in.
            return false
        }
        let userID = Auth.auth().currentUser?.uid
        ref.child("players").child(userID!).child("coords").setValue(["longitude": longitude, "latitude": latitude, "altitude": altitude])
        return true
    }
    
    public func addPlayer() -> Bool {
        if Auth.auth().currentUser == nil {
            // User is not signed in.
            return false
        }
        let userID = Auth.auth().currentUser?.uid
        //ref.child("players").child(userID!).setValue(["name": name])
        ref.child("players").child(userID!).child("coords").setValue(["latitude": 0.0, "longitude": 0.0, "altitude": 0.0])
        return true
    }
    
    private func removePlayer(uid: String) -> Bool {
        if Auth.auth().currentUser == nil {
            // User is not signed in.
            return false
        }
        removePlayerFromFights(uid: uid)
        ref.child("players").child(uid).removeValue()
        return true
    }
    
    private func removePlayerFromFights(uid: String) {
        ref.child("players").child(uid).child("fight").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let bossName = snapshot.value as! String
                self.ref.child("fights").child(bossName).child("players").child(uid).removeValue()
            }
        })
    }
    
    public func removeFightFromCurrentUser() {
        if Auth.auth().currentUser != nil {
            let userID = Auth.auth().currentUser?.uid
            self.ref.child("players").child(userID!).child("fight").removeValue()
        }
    }
    
    public func isUserInFight() -> Bool {
        if Auth.auth().currentUser == nil {
            // User is not signed in.
            return false
        }
        let userID = Auth.auth().currentUser?.uid
        var userIsInFight = false
        ref.child("players").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            userIsInFight = snapshot.hasChild("fight")
        })
        return userIsInFight
    }
    
    public func getUserID() -> String {
        if let uid = Auth.auth().currentUser?.uid {
            return uid
        }
        return ""
    }
    
    public func notifyAttackIsOver(boss: String) {
        ref.child("bosses").child(boss).child("target").removeValue()
        print("Boss \(boss) was notified.")
    }
    
    
    public func updateUsername(name: String) -> Bool {
        if Auth.auth().currentUser == nil {
            // User is not signed in.
            return false
        }
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = name
        changeRequest?.commitChanges { (error) in
        }
        return true
    }
    
    public func decrementHpFrom(boss: String, hp: Int) {
        let hpAsString = String(hp)
        let queryItems = [NSURLQueryItem(name: "boss", value: boss), NSURLQueryItem(name: "dec", value: hpAsString)]
        
        let urlComps = NSURLComponents(string: "https://us-central1-seen-d0f3b.cloudfunctions.net/decBossHp")!
        urlComps.queryItems = queryItems as [URLQueryItem]
        
        let task = URLSession.shared.dataTask(with: urlComps.url!) {(data, response, error) in
        }
        task.resume()
    }
}
