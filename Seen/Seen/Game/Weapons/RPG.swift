//
//  RPG.swift
//  Seen
//
//  Created by Wolfgang on 27.11.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit
import ARCL

class RPG : Weapon {
    private let rpgNode: SCNNode!
    private let rocketNode: SCNNode!
    var delegate: RGPReloadAnimatorDelegate?
    
    var isShootingAllowed = true {
        didSet { rocketNode.isHidden = !isShootingAllowed }
    }
    
    override init(sceneLocationView: SceneLocationView, assets: Assets) {
        rpgNode = assets.rpgNode.clone()
        rocketNode = rpgNode.childNode(withName: "rocket", recursively: true)!
        super.init(sceneLocationView: sceneLocationView, assets: assets)
        
        rpgNode.position = SCNVector3Make(0.1, 0, -0.2)
        
        isShootingAllowed = true
    }
    
    public override func activate() {
        sceneLocationView.pointOfView!.addChildNode(rpgNode)
    }
    
    override func deactivate() {
        rpgNode.removeFromParentNode()
    }
    
    func shoot() {
        isShootingAllowed = false
        
        let missile = rocketNode.clone()
        missile.isHidden = false
        Weapon.initMissilePhysicsBody(missile)
        rpgNode.addChildNode(missile)
        
        let directionScale: Float = 100
        let wf = rpgNode.worldFront
        let direction = SCNVector3Make(wf.x * directionScale, wf.y * directionScale, wf.z * directionScale)
        missile.physicsBody!.applyForce(direction, asImpulse: true)
        
        missile.addParticleSystem(assets.smokeSystem)
        
        runReloadAction(timeInterval: 3)
        
        missile.removeFromParentNode(delay: 5)
    }
    
    private func runReloadAction(timeInterval: Double) {
        let reloadAnimationAction = SCNAction.run { _ in
            self.delegate?.startRPGReloadAnimation()
        }
        let waitAction = SCNAction.wait(duration: timeInterval)
        let reloadAction = SCNAction.run { _ in
            self.reload()
            self.delegate?.stopRPGReloadAnimation()
        }
        let actionSequence = SCNAction.sequence([reloadAnimationAction, waitAction, reloadAction])
        rpgNode.runAction(actionSequence, forKey: "reload")
    }
    
    private func reload() {
        self.isShootingAllowed = true
    }
    
    override func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        if(contact.nodeA.physicsBody!.categoryBitMask == Categories.boss &&
            contact.nodeB.physicsBody!.categoryBitMask == Categories.missile) {
            
            let hitNode = replaceMissileWithHitNode(contact: contact)
            hitNode.addParticleSystem(assets.explosionSmallSystem)
        }
    }
}

protocol RGPReloadAnimatorDelegate {
    func startRPGReloadAnimation()
    func stopRPGReloadAnimation()
}
