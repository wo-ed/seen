//
//  Slingshot.swift
//  Seen
//
//  Created by Wolfgang on 27.11.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import Foundation
import ARCL
import ARKit

class Slingshot : Weapon {
    // The size of the 3d model scene
    private let originalStoneSize: Float = 1.731
    
    private let stoneSize: Float = 0.5
    
    private var targetNode: SCNNode!
    
    private let staticFork: SCNNode!
    
    private let forkScale: Float = 0.025
    private var directionYAdd: Float = 0.0
    private var targetPositionYAdd: Float = 0.0
    
    var isTargeting = false {
        didSet {
            targetNode.isHidden = !isTargeting
            staticFork.isHidden = isTargeting
        }
    }
    
    private var isTargetingAllowed = true
    
    override init(sceneLocationView: SceneLocationView, assets: Assets) {
        self.staticFork = assets.forkNode.clone()
        super.init(sceneLocationView: sceneLocationView, assets: assets)
        
        staticFork.scale = SCNVector3(float3(forkScale))
        staticFork.position = SCNVector3Make(-0.002, -0.04, -0.1)
        
        initFork()
        //initCrosshair()
    }
    
    private func initFork() {
        let forkNode = staticFork.clone()
        forkNode.scale = SCNVector3(float3(forkScale))
        let constraint = SCNLookAtConstraint(target: sceneLocationView.pointOfView!)
        constraint.isGimbalLockEnabled = true
        forkNode.constraints = [constraint]
     
        targetNode = forkNode
        directionYAdd = 1.5 * forkScale
        targetPositionYAdd = -0.75 * forkScale
    }
    
    private func initCrosshair() {
         let plane = SCNPlane(width: 0.05, height: 0.05)
         plane.firstMaterial!.diffuse.contents = assets.crosshairImg
         plane.firstMaterial!.lightingModel = .constant
         let crosshairNode = SCNNode(geometry: plane)
         crosshairNode.constraints = [SCNBillboardConstraint()]
        
        targetNode = crosshairNode
    }
    
    override func activate() {
        sceneLocationView.sceneNode!.addChildNode(targetNode)
        sceneLocationView.pointOfView!.addChildNode(staticFork)
        
        isTargeting = false
    }
    
    override func deactivate() {
        targetNode.removeFromParentNode()
        staticFork.removeFromParentNode()
    }
    
    func startTargeting() {
        isTargeting = true
        targetNode.position = sceneLocationView.pointOfView!.position
        targetNode.position.y += targetPositionYAdd
    }
    
    func shoot() {
        let stoneScale: Float = stoneSize / originalStoneSize
        
        let missile = assets.stoneNode.clone()
        missile.scale = SCNVector3(float3(stoneScale))
        missile.position = sceneLocationView.pointOfView!.position
        Weapon.initMissilePhysicsBody(missile)
        sceneLocationView.sceneNode!.addChildNode(missile)
        
        let directionScale: Float = 200 * stoneSize
        
        let tp = targetNode.position
        let mp = missile.position
        
        let direction = SCNVector3Make(tp.x - mp.x, tp.y - mp.y + directionYAdd, tp.z - mp.z)
        let scaledDirection = SCNVector3Make(
            direction.x * directionScale,
            direction.y * directionScale,
            direction.z * directionScale)
        
        missile.physicsBody!.applyForce(scaledDirection, asImpulse: true)
        
        // Just some random values for the torque effect
        missile.physicsBody!.applyTorque(
            SCNVector4Make(1 * stoneSize,
                           1 * stoneSize,
                           2 * stoneSize,
                           2 * stoneSize),
            asImpulse: true)
        
        isTargeting = false
        
        missile.removeFromParentNode(delay: 5)
    }
}
