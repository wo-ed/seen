//
//  Weapon.swift
//  Seen
//
//  Created by Wolfgang on 27.11.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit
import ARCL

class Weapon {
    let sceneLocationView: SceneLocationView!
    let assets: Assets!
    
    init(sceneLocationView: SceneLocationView, assets: Assets) {
        self.sceneLocationView = sceneLocationView
        self.assets = assets
    }
    
    public func activate() {}
    public func deactivate() {}

    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {}
    
    func replaceMissileWithHitNode(contact: SCNPhysicsContact) -> SCNNode {
        contact.nodeB.isHidden = true
        let hitNode = SCNNode()
        hitNode.position = contact.contactPoint
        sceneLocationView.scene.rootNode.addChildNode(hitNode)
        return hitNode
    }
    
    static func initMissilePhysicsBody(_ missile: SCNNode) {
        missile.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        missile.physicsBody!.categoryBitMask = Categories.missile
        missile.physicsBody!.collisionBitMask = Categories.boss
        missile.physicsBody!.contactTestBitMask = Categories.boss
    }
}
