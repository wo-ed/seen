//
//  Dragon.swift
//  Seen
//
//  Created by Wolfgang on 02.12.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit
import ARCL
import CoreLocation

class Dragon {
    
    // The root node contains the bones, the full animation and the dragon
    let rootNode: SCNNode!
    
    private let sceneLocationView: SceneLocationView
    private let assets: Assets
    private let scale: Float
    private var targetPosition = SCNVector3Zero
    
    private let mouthNode = SCNNode()
    private var otherPlayerNode: LocationNode!
    private var fullPlayer: SCNAnimationPlayer!
    private var fullAnimation: CAAnimation!
    private var bone: SCNNode!
    
    public var locationNode: Location3DNode?
    public let bossName: String
    
    // The size of the 3d model scene
    private let originalDragonSize: Float = 85.14
    
    private let dragonSize: Float = 10
    
    public let fireballAttackDelay = 7.0 // seconds
    private static let animationFps = 25.0
    
    init(_ bossName: String, _ assets: Assets, _ sceneLocationView: SceneLocationView) {
        self.bossName = bossName
        self.assets = assets
        self.sceneLocationView = sceneLocationView
        self.scale = dragonSize / originalDragonSize
        self.rootNode = assets.dragonScene.rootNode.clone()
        
        initAnimations()
        initAttackTargetNode()
        
        rootNode.scale = SCNVector3(float3(scale))
        
        // Fix the rotation axis of the dragon model
        let degrees: Float = 180
        rootNode.pivot = SCNMatrix4MakeRotation(degrees * .pi / 180, 0, 1, 0)
        
        turnToTarget(targetNode: otherPlayerNode!)

        initPhysicsBody()
        
        rootNode.addChildNode(mouthNode)
    }
    
    private func initAttackTargetNode() {
        let coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        otherPlayerNode = LocationNode(location: CLLocation(coordinate: coordinate, altitude: 0))
        sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: otherPlayerNode!)
    }
    
    // The animations must be extracted from the full animation
    private func initAnimations() {
        bone = rootNode.childNode(withName: "Bone", recursively: true)!
        
        // Extract the full animation from the bone
        fullPlayer = bone.animationPlayer(forKey: "anim")!
        fullAnimation = CAAnimation(scnAnimation: fullPlayer.animation)
        fullPlayer.stop()
        
        startFlyAnimation()
    }
    
    private func turnToTarget(targetNode: SCNNode) {
        let constraint = SCNLookAtConstraint(target: targetNode)
        constraint.isGimbalLockEnabled = true
        rootNode.constraints = [constraint]
    }
    
    private func startFlyAnimation() {
        let flyAnimation = fullAnimation.subAnimation(startingAtFrame: 268, endingAtFrame: 288, fps: Dragon.animationFps)
        flyAnimation.repeatCount = .greatestFiniteMagnitude
        flyAnimation.fadeOutDuration = 1
        
        let flyPlayer = SCNAnimationPlayer(animation: SCNAnimation(caAnimation: flyAnimation))
        bone.addAnimationPlayer(flyPlayer, forKey: "fly")
        
        flyPlayer.play()
    }
    
    public func attackPlayer() {
        let targetNode = sceneLocationView.pointOfView!
        self.targetPosition = targetNode.position
        
        turnToTarget(targetNode: targetNode)
        
        startFireAnimation()
    }
    
    public func attackOtherPlayer(latitude lat: Double, longitude lng: Double, altitude alt: Double) {
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        otherPlayerNode!.location = CLLocation(coordinate: coordinate, altitude: alt)
        sceneLocationView.updatePositionAndScaleOfLocationNode(locationNode: otherPlayerNode)
        turnToTarget(targetNode: otherPlayerNode)
        
        self.targetPosition = otherPlayerNode.position
        startFireAnimation()
    }
    
    private func startFireAnimation() {
        let fireAnimation = fullAnimation.subAnimation(startingAtFrame: 289, endingAtFrame: 353, fps: Dragon.animationFps)
        fireAnimation.repeatCount = 1
        fireAnimation.fadeInDuration = 1
        
        let firePlayer = SCNAnimationPlayer(animation: SCNAnimation(caAnimation: fireAnimation))
        bone.addAnimationPlayer(firePlayer, forKey: "fire")
        firePlayer.play()
        
        firePlayer.animation.animationDidStop = fireAnimationDidStop
    }
    
    private func fireAnimationDidStop(animation: SCNAnimation, id receiver: SCNAnimatable, completed: Bool) {
        shootFireball(targetPosition: self.targetPosition)
    }
    
    public func shootFireball(targetPosition: SCNVector3){
        let fireballNode = SCNNode(geometry: SCNSphere(radius: 0.1))
        fireballNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        fireballNode.physicsBody!.isAffectedByGravity = false
        
        fireballNode.physicsBody!.categoryBitMask = Categories.fireball
        fireballNode.physicsBody!.collisionBitMask = Categories.playerBody
        fireballNode.physicsBody!.contactTestBitMask = Categories.playerBody
        
        // Set fireball position relative to the dragon
        if(locationNode == nil) {
            mouthNode.position.y = 3.7
            let absoluteNode = rootNode!
            fireballNode.position = mouthNode.convertPosition(absoluteNode.position, to: absoluteNode)
        } else {
            mouthNode.position.y = 35
            let absoluteNode = sceneLocationView.scene.rootNode
            fireballNode.position = mouthNode.convertPosition(absoluteNode.position, to: absoluteNode)
        }
        
        sceneLocationView.sceneNode!.addChildNode(fireballNode)

        let velocity: Float = 1.5
        let start = fireballNode.position
        
        let direction = SCNVector3Make(targetPosition.x - start.x, targetPosition.y - start.y, targetPosition.z - start.z)
        let scaledDirection = SCNVector3Make(direction.x * velocity, direction.y * velocity, direction.z * velocity)

        fireballNode.physicsBody!.applyForce(scaledDirection, asImpulse: true)
        
        fireballNode.addParticleSystem(assets.fireball)
        
        fireballNode.removeFromParentNode(delay: 5)
    }
    
    private func initPhysicsBody() {
        let node = rootNode.childNode(withName: "dragon", recursively: true)!
        
        // Using a box as physics shape since the shape of the 3d model does not work properly
        let boxSize = CGFloat(dragonSize / 4)
        let geometry = SCNBox(width: boxSize, height: boxSize, length: boxSize, chamferRadius: 0)
        
        // Physics shape scale
        // let options: [SCNPhysicsShape.Option : Any]? = [SCNPhysicsShape.Option.scale: scale]
        
        let shape = SCNPhysicsShape(geometry: geometry, options: nil)
        node.physicsBody = SCNPhysicsBody(type: .kinematic, shape: shape)
        node.physicsBody!.categoryBitMask = Categories.boss
        node.physicsBody!.collisionBitMask = Categories.missile
    }
    
    // Makes the dragon attack the player repeatedly
    func startDemoAttack(timeInterval: Double) {
        let waitAction = SCNAction.wait(duration: timeInterval)
        let attackAction = SCNAction.run { _ in self.attackPlayer() }
        let actionSequence = SCNAction.sequence([waitAction, attackAction])
        let repeatedActionSequence = SCNAction.repeatForever(actionSequence)
        rootNode.runAction(repeatedActionSequence, forKey: "demoAttack")
    }
}
