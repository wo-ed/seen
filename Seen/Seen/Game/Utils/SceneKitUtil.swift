//
//  SCNNodeExtensions.swift
//  Seen
//
//  Created by Wolfgang on 28.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit

extension SCNNode {
    func removeFromParentNode(delay: Double) {
        let waitAction = SCNAction.wait(duration: delay)
        let removeAction = SCNAction.run { $0.removeFromParentNode() }
        let actionSequence = SCNAction.sequence([waitAction, removeAction])
        self.runAction(actionSequence)
    }
}
