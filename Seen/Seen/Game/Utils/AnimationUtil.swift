//
//  AnimationUtil.swift
//  Seen
//
//  Created by Wolfgang on 02.12.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit

func timeRange(forStartingAtFrame start: Int, endingAtFrame end: Int, fps: Double = 30) -> (offset: TimeInterval, duration: TimeInterval) {
    
    let startTime = time(atFrame: start, fps: fps)
    let endTime = time(atFrame: end, fps: fps)
    return (offset:startTime, duration:endTime - startTime)
}

func time(atFrame frame: Int, fps: Double = 30) -> TimeInterval {
    return TimeInterval(frame) / fps
}

extension CAAnimation {
    func subAnimation(startingAtFrame start: Int, endingAtFrame end: Int, fps: Double = 30) -> CAAnimation {
        let range = timeRange(forStartingAtFrame: start, endingAtFrame: end, fps: fps)
        let animation = CAAnimationGroup()
        let sub = self.copy() as! CAAnimation
        sub.timeOffset = range.offset
        animation.animations = [sub]
        animation.duration = range.duration
        return animation
    }
}
