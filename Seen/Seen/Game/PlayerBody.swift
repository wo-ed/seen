//
//  playerBody.swift
//  Seen
//
//  Created by admin on 16.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit
import ARCL

class PlayerBody {
    private let bodyNode: SCNNode
    private let sceneLocationView: SceneLocationView!
    private let assets: Assets
    
    init(sceneLocationView: SceneLocationView, assets: Assets){
        self.sceneLocationView = sceneLocationView
        self.assets = assets
        
        bodyNode = SCNNode(geometry: SCNBox(width: 0.6, height: 1, length: 1, chamferRadius: 0))
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.clear
        bodyNode.geometry!.materials = [material]
        
        bodyNode.physicsBody = SCNPhysicsBody(type: .kinematic, shape: nil)
        
        bodyNode.position = SCNVector3Make(0, 0, -0.75)
        sceneLocationView.pointOfView!.addChildNode(bodyNode)
        
        initPhysicsBody()
    }
    
    private func initPhysicsBody() {
        bodyNode.physicsBody!.categoryBitMask = Categories.playerBody
        bodyNode.physicsBody!.collisionBitMask = Categories.fireball
    }
    
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        
        // Checks if the player was shot with a fireball and shows an explosion
        if(contact.nodeA.physicsBody!.categoryBitMask == Categories.playerBody &&
            contact.nodeB.physicsBody!.categoryBitMask == Categories.fireball) {
            
            let hitNode = replaceFireballWithHitNode(contact: contact)
            hitNode.addParticleSystem(assets.explosionSmallSystem)
        }
    }
    
    func physicsWorld(_ world: SCNPhysicsWorld, didEnd contact: SCNPhysicsContact) {}
    
    func physicsWorld(_ world: SCNPhysicsWorld, didUpdate contact: SCNPhysicsContact) {}
    
    func replaceFireballWithHitNode(contact: SCNPhysicsContact) -> SCNNode {
        contact.nodeB.isHidden = true
        let hitNode = SCNNode()
        hitNode.position = contact.contactPoint
        sceneLocationView.scene.rootNode.addChildNode(hitNode)
        return hitNode
    }
}
