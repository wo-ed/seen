import Foundation
import SceneKit
import CoreLocation
import ARCL

open class Location3DNode: LocationNode {
    ///Subnodes and adjustments should be applied to this subnode
    public let annotationNode: SCNNode
    
    public init(location: CLLocation?, annotationNode: SCNNode) {
        self.annotationNode = annotationNode
        super.init(location: location)
        addChildNode(annotationNode)
    }
    
    convenience public init(location: CLLocation?, geometry: SCNGeometry) {
        self.init(location: location, annotationNode: SCNNode(geometry: geometry))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
