//
//  lifebarScene.swift
//  Seen
//
//  Created by admin on 14.01.18.
//  Copyright © 2018 Jowojomo. All rights reserved.
//


import UIKit
import SpriteKit

class LifebarScene: SKScene {
    
    var pauseNode: SKSpriteNode!
    var scoreNode: SKLabelNode!
    var playerBar: LifeBar!
    var bossBar: LifeBar!
    
    var score = 0 {
        didSet {
            self.scoreNode.text = "Score: \(self.score)"
        }
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.backgroundColor = UIColor.clear
        initLifebar(0)
        initBossbar(0)
        hideBossBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func initLifebar(_ newLifeAsInt: Int){
        let spriteSize = size.width/12
        playerBar = LifeBar(color: SKColor.green, size:CGSize(width:100, height:10))
        playerBar.position = CGPoint(x: spriteSize + 30, y: spriteSize + 550)
        addChild(playerBar)
        let newLifeAsFloat = Float(newLifeAsInt)
        let newLifeAsCGFloat = CGFloat(newLifeAsFloat/100)
        let newLife = playerBar.xScale - newLifeAsCGFloat
        if(newLifeAsInt == 0){
            playerBar.progress = 1.0
        }else{
            playerBar.progress = newLife
        }
    }
    
    public func updateLifebar(_ newLifeAsInt: Int){
        playerBar.isHidden = true
        let newLifeAsFloat = Float(newLifeAsInt)
        let newLifeAsCGFloat = CGFloat(newLifeAsFloat/100)
        let newLife = playerBar.xScale - newLifeAsCGFloat
        playerBar.progress = newLife
        playerBar.isHidden = false
    }
    
    public func initBossbar(_ newLifeAsInt: Int){
        let spriteSize = size.width/12
        bossBar = LifeBar(color: SKColor.red, size:CGSize(width:180, height:10))
        bossBar.position = CGPoint(x: spriteSize + 250, y: spriteSize + 550)
        addChild(bossBar)
        let newLifeAsCGFloat = CGFloat(newLifeAsInt/100)
        if(newLifeAsInt == 0){
        bossBar.progress = 1.0
        }else{
            bossBar.progress = bossBar.progress - newLifeAsCGFloat
        }
    }
    
    public func updateBossbar(_ newLifeAsInt: Int){
        bossBar.isHidden = true
        let newLifeAsFloat = Float(newLifeAsInt)
        let newLifeAsCGFloat = CGFloat(newLifeAsFloat/100)
        let newLife = playerBar.xScale - newLifeAsCGFloat
        bossBar.progress = newLife
        bossBar.isHidden = false
    }
    
    public func hideBossBar(){
        bossBar.isHidden = true
    }
    
    public func showBossBar(){
        bossBar.isHidden = false
    }}
