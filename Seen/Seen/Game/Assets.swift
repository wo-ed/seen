//
//  Assets.swift
//  Seen
//
//  Created by Wolfgang on 28.11.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

import Foundation
import SceneKit

class Assets {
    // Scenes
    public let dragonScene = SCNScene(named: "art.scnassets/falcias.dae")!
    public let rpgScene = SCNScene(named: "art.scnassets/rpg.dae")!
    public let stoneScene = SCNScene(named: "art.scnassets/Rock.obj")!
    public let slingShotScene = SCNScene(named: "art.scnassets/SlingShot2.obj")!
    
    // Nodes
    public let rpgNode: SCNNode!
    public let stoneNode: SCNNode!
    public let forkNode: SCNNode!
    
    // Images
    public let crosshairImg = UIImage(named: "art.scnassets/triangle-06-whole.png")!
    public let stoneImg = UIImage(named: "art.scnassets/RockRed0058_L.jpg")!
    
    // Particle effects
    public let smokeSystem = SCNParticleSystem(named: "smoke.scnp", inDirectory: "art.scnassets")!
    public let explosionBigSystem = SCNParticleSystem(named: "explosion_big.scnp", inDirectory: "art.scnassets")!
    public let explosionSmallSystem = SCNParticleSystem(named: "explosion_small.scnp", inDirectory: "art.scnassets")!
    public let fireball = SCNParticleSystem(named: "fireball.scnp", inDirectory: "art.scnassets")!
    public let icefragment = SCNParticleSystem(named: "fireball_dark.scnp", inDirectory: "art.scnassets")!
    
    init() {
        rpgNode = rpgScene.rootNode.childNode(withName: "rpg", recursively: true)
        
        stoneNode = stoneScene.rootNode.childNode(withName: "Cube.006_Cube.009_Rock.003", recursively: true)
        stoneNode.geometry!.materials.first!.diffuse.contents = stoneImg
        
        forkNode = slingShotScene.rootNode.childNode(withName: "Fork_default", recursively: true)
        forkNode.geometry!.materials.first!.diffuse.contents = UIColor(red: 61 / 255, green: 24 / 255, blue: 0, alpha: 1)
    }
}
