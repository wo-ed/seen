//
//  Constants.swift
//  Seen
//
//  Created by Wolfgang on 27.11.17.
//  Copyright © 2017 Jowojomo. All rights reserved.
//

struct Categories {
    public static let boss = 0x1 << 1
    public static let missile = 0x1 << 2
    public static let playerBody = 0x1 << 3
    public static let fireball = 0x1 << 4
}

// Modes for the boss at a static position
enum BossTestMode {
    case hidden
    case visible
    case attackingPlayer
    //case attackingAll
}
